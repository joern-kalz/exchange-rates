package rates.service;

import static org.mockito.Mockito.verify;

import java.time.LocalDate;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.springframework.web.client.RestTemplate;

public class RateServiceTest {
    
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @InjectMocks
    RateService rateService;
    
    @Mock
    RestTemplate restTemplate;
    
    @Test
    public void shouldGetRateInterval() {
        rateService.getRateInterval(LocalDate.of(2019, 01, 01), LocalDate.of(2019, 02, 01), Currency.EUR, 
                Currency.USD);
        
        verify(restTemplate).getForObject("https://api.exchangeratesapi.io/history?"
                + "start_at={start}&end_at={end}&base={base}&symbols={target}", RateInterval.class, 
                "2019-01-01", "2019-02-01", "EUR", "USD");
    }
}
