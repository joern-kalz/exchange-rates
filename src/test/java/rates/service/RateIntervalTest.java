package rates.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class RateIntervalTest {
    
    @Test
    public void shouldUnmarshall() throws JsonMappingException, JsonProcessingException {
        RateInterval interval = new ObjectMapper().readerFor(RateInterval.class).readValue(
                "{\"rates\":{\"2019-09-10\":{\"USD\":1.104}},\"start_at\":\"2019-09-10\",\"base\":\"EUR\"," + 
                "\"end_at\":\"2019-09-10\"}");
        
        assertEquals(interval.getRates().get("2019-09-10").get(Currency.USD), 1.104);
    }

}
