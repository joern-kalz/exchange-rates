package rates.instant;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;

import rates.service.Currency;
import rates.service.RateInterval;

public class RateInstantBuilderTest {

    RateInstantBuilder rateInstantBuilder = new RateInstantBuilder();
    
    @Test
    public void shouldSelectLastRate() throws InvalidRateIntervalException {
        RateInterval interval = createTestInterval();
        RateInstant result = rateInstantBuilder.build(interval, Currency.EUR);
        assertEquals(7.0, result.getRate());
    }
    
    @Test
    public void shouldCalculateRunningAverage() throws InvalidRateIntervalException {
        RateInterval interval = createTestInterval();
        RateInstant result = rateInstantBuilder.build(interval, Currency.EUR);
        assertEquals(4.0, result.getRunningAverage());
    }
    
    @Test
    public void shouldCalculateTrend() throws InvalidRateIntervalException {
        RateInterval interval = createTestInterval();
        RateInstant result = rateInstantBuilder.build(interval, Currency.EUR);
        assertEquals(Trend.ASCENDING, result.getTrend());
    }
    
    private RateInterval createTestInterval() {
        Map<String, Map<Currency, Double>> rates = new HashMap<>();
        rates.put("2019-01-01", Collections.singletonMap(Currency.EUR, 2.0));
        rates.put("2019-01-02", Collections.singletonMap(Currency.EUR, 3.0));
        rates.put("2019-01-05", Collections.singletonMap(Currency.EUR, 6.0));
        rates.put("2019-01-03", Collections.singletonMap(Currency.EUR, 4.0));
        rates.put("2019-01-06", Collections.singletonMap(Currency.EUR, 7.0));
        rates.put("2019-01-04", Collections.singletonMap(Currency.EUR, 5.0));
        
        RateInterval interval = new RateInterval();
        interval.setRates(rates);

        return interval;
    }
}
