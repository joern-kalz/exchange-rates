package rates.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.RestClientException;

import rates.instant.RateInstantBuilder;
import rates.instant.RateInstantRepository;
import rates.service.Currency;
import rates.service.RateService;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
public class RateControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    RateService rateService;
    
    @MockBean
    RateInstantBuilder rateInstantBuilder;
    
    @MockBean
    RateInstantRepository rateInstantRepository;

    @Test
    public void shouldGetRateInstant() throws Exception {
        this.mvc.perform(get("/api/exchange-rate/2019-02-01/EUR/USD"))
            .andExpect(status().isOk());
        
        verify(rateService).getRateInterval(LocalDate.of(2019, 01, 02), LocalDate.of(2019, 02, 01), 
                Currency.EUR, Currency.USD);
    }

    @Test
    public void shouldReturnBadRequestForInvalidDate() throws Exception {
        this.mvc.perform(get("/api/exchange-rate/2019-13-01/EUR/USD"))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldReturnBadRequestForDateOutOfRange() throws Exception {
        this.mvc.perform(get("/api/exchange-rate/1999-12-01/EUR/USD"))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldReturnServerErrorForInvalidRateServiceResponse() throws Exception {
        when(rateService.getRateInterval(any(LocalDate.class), any(LocalDate.class), any(Currency.class), 
                any(Currency.class))).thenThrow(RestClientException.class);
        
        this.mvc.perform(get("/api/exchange-rate/2019-02-01/EUR/USD"))
            .andExpect(status().is5xxServerError());
    }

    @Test
    public void shouldGetRateInstantsForDay() throws Exception {
        this.mvc.perform(get("/api/exchange-rate/history/daily/2002/01/01"))
            .andExpect(status().isOk());
        
        verify(rateInstantRepository).findByDateGreaterThanEqualAndDateLessThanEqual("2002-01-01", "2002-01-01");
    }

    @Test
    public void shouldGetRateInstantsForMonth() throws Exception {
        this.mvc.perform(get("/api/exchange-rate/history/monthly/2002/03"))
            .andExpect(status().isOk());
        
        verify(rateInstantRepository).findByDateGreaterThanEqualAndDateLessThanEqual("2002-03-01", "2002-03-31");
    }


}
