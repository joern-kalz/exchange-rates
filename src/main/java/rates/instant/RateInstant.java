package rates.instant;

import javax.persistence.Entity;
import javax.persistence.Id;

import rates.service.Currency;

@Entity
public class RateInstant {

    @Id
    private String id;
    private String date;
    private Currency base;
    private Currency target;
    private double rate;
    private double runningAverage;
    private Trend trend;
    
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    public Currency getBase() {
        return base;
    }
    public void setBase(Currency base) {
        this.base = base;
    }
    public Currency getTarget() {
        return target;
    }
    public void setTarget(Currency target) {
        this.target = target;
    }
    public double getRate() {
        return rate;
    }
    public void setRate(double rate) {
        this.rate = rate;
    }
    public double getRunningAverage() {
        return runningAverage;
    }
    public void setRunningAverage(double runningAverage) {
        this.runningAverage = runningAverage;
    }
    public Trend getTrend() {
        return trend;
    }
    public void setTrend(Trend trend) {
        this.trend = trend;
    }
    
}
