package rates.instant;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface RateInstantRepository extends CrudRepository<RateInstant, String> {

    List<RateInstant> findByDateGreaterThanEqualAndDateLessThanEqual(String start, String end);
}
