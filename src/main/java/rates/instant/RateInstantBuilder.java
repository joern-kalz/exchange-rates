package rates.instant;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Map.Entry;

import org.springframework.stereotype.Component;

import rates.service.Currency;
import rates.service.RateInterval;

@Component
public class RateInstantBuilder {

    public static final int MINIMAL_INTERVAL_LENGTH_IN_DAYS = 30;

    private static final int RUNNING_AVERAGE_COUNT = 5;

    public RateInstant build(RateInterval interval, Currency currency) throws InvalidRateIntervalException {
        List<Double> sortedRates = sortRatesByDate(interval, currency);

        if (sortedRates.size() < RUNNING_AVERAGE_COUNT + 1) {
            throw new InvalidRateIntervalException("interval is too short");
        }

        RateInstant result = new RateInstant();
        result.setId(createId(interval, currency));
        result.setDate(interval.getEnd());
        result.setBase(interval.getBase());
        result.setTarget(currency);
        result.setRate(sortedRates.get(sortedRates.size() - 1));
        result.setRunningAverage(calculateRunningAverage(sortedRates));
        result.setTrend(calculateTrend(sortedRates));

        return result;
    }

    private List<Double> sortRatesByDate(RateInterval interval, Currency currency)
            throws InvalidRateIntervalException {
        SortedMap<String, Double> ratesByDate = new TreeMap<>();

        for (Entry<String, Map<Currency, Double>> datedRate : interval.getRates().entrySet()) {
            String date = datedRate.getKey();
            Double rate = datedRate.getValue().get(currency);

            if (rate == null) {
                throw new InvalidRateIntervalException("currency " + currency + " not found at " + date);
            }

            ratesByDate.put(date, rate);
        }

        return new ArrayList<Double>(ratesByDate.values());
    }
    
    private String createId(RateInterval interval, Currency currency) {
        return interval.getEnd() + "-" + interval.getBase() + "-" + currency;
    }

    private double calculateRunningAverage(List<Double> sortedRates) {
        int size = sortedRates.size();

        return sortedRates
                .subList(size - RUNNING_AVERAGE_COUNT - 1, size - 1)
                .stream()
                .mapToDouble(i -> i)
                .average()
                .getAsDouble();
    }

    private Trend calculateTrend(List<Double> sortedRates) {
        boolean descending = true;
        boolean ascending = true;
        boolean constant = true;

        int size = sortedRates.size();

        for (int i = size - RUNNING_AVERAGE_COUNT - 1; i < size - 1; i++) {
            if (sortedRates.get(i) >= sortedRates.get(i + 1)) {
                ascending = false;
            }

            if (sortedRates.get(i) <= sortedRates.get(i + 1)) {
                descending = false;
            }

            if (sortedRates.get(i) != sortedRates.get(i + 1)) {
                constant = false;
            }
        }

        return descending ? Trend.DESCENDING : 
            ascending ? Trend.ASCENDING : 
            constant ? Trend.CONSTANT :
            Trend.UNDEFINED;
    }
}
