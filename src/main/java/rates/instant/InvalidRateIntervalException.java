package rates.instant;

public class InvalidRateIntervalException extends Exception {

    private static final long serialVersionUID = 1L;

    public InvalidRateIntervalException(String message) {
        super(message);
    }

}
