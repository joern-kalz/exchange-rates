package rates.instant;

public enum Trend {
    DESCENDING, ASCENDING, CONSTANT, UNDEFINED
}
