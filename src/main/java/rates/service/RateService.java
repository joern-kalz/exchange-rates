package rates.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class RateService {

    private static final String HISTORY_URL = "https://api.exchangeratesapi.io/history?"
            + "start_at={start}&end_at={end}&base={base}&symbols={target}";

    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    @Autowired
    private RestTemplate restTemplate;

    public RateInterval getRateInterval(LocalDate start, LocalDate end, Currency base, Currency target) {
        String startString = DATE_FORMATTER.format(start);
        String endString = DATE_FORMATTER.format(end);
        String baseString = base.toString();
        String targetString = target.toString();

        return restTemplate.getForObject(HISTORY_URL, RateInterval.class, startString, endString, baseString,
                targetString);
    }

}
