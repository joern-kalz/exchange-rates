package rates.service;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RateInterval {

    private Currency base;

    @JsonProperty("start_at")
    private String start;

    @JsonProperty("end_at")
    private String end;

    private Map<String, Map<Currency, Double>> rates;

    public Currency getBase() {
        return base;
    }

    public void setBase(Currency base) {
        this.base = base;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public Map<String, Map<Currency, Double>> getRates() {
        return rates;
    }

    public void setRates(Map<String, Map<Currency, Double>> rates) {
        this.rates = rates;
    }

}
