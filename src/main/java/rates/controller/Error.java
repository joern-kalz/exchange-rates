package rates.controller;

public class Error {

    int status;
    String title;
    String detail;

    public Error() {
    }

    public Error(int status, String title, String detail) {
        this.status = status;
        this.title = title;
        this.detail = detail;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

}
