package rates.controller;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.TemporalAdjusters;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import rates.instant.InvalidRateIntervalException;
import rates.instant.RateInstant;
import rates.instant.RateInstantBuilder;
import rates.instant.RateInstantRepository;
import rates.service.Currency;
import rates.service.RateInterval;
import rates.service.RateService;

@RestController
@RequestMapping("/api/exchange-rate")
public class RatesController {

    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private static final LocalDate DATE_MIN = LocalDate.of(2000, 01, 01);

    @Autowired
    RateService rateService;
    
    @Autowired
    RateInstantBuilder rateInstantBuilder;
    
    @Autowired
    RateInstantRepository rateInstantRepository;

    @GetMapping("{date}/{baseCurrency}/{targetCurrency}")
    public RateInstant getRateInstant(@PathVariable String date, @PathVariable Currency baseCurrency,
            @PathVariable Currency targetCurrency) throws InvalidRateIntervalException, DateOutOfRangeException 
    {
        LocalDate end = parseDate(date);
        LocalDate start = end.minusDays(RateInstantBuilder.MINIMAL_INTERVAL_LENGTH_IN_DAYS);
        RateInterval interval = rateService.getRateInterval(start, end, baseCurrency, targetCurrency);
        
        RateInstant rateInstant = rateInstantBuilder.build(interval, targetCurrency);
        rateInstantRepository.save(rateInstant);
        
        return rateInstant;
    }
    
    @GetMapping("history/daily/{year}/{month}/{day}")
    public List<RateInstant> getRateInstantsForDay(@PathVariable String year, @PathVariable String month, 
            @PathVariable String day) throws ParseException, DateOutOfRangeException 
    {
        String date = year + "-" + month + "-" + day;
        parseDate(date);
        
        return rateInstantRepository.findByDateGreaterThanEqualAndDateLessThanEqual(date, date);
    }
    
    @GetMapping("history/monthly/{year}/{month}")
    public List<RateInstant> getRateInstantsForMonth(@PathVariable String year, @PathVariable String month) 
            throws ParseException, DateOutOfRangeException 
    {
        String date = year + "-" + month + "-01";
        LocalDate start = parseDate(date);
        LocalDate end = start.with(TemporalAdjusters.lastDayOfMonth());
        String startString = DATE_FORMATTER.format(start);
        String endString = DATE_FORMATTER.format(end);
        
        return rateInstantRepository.findByDateGreaterThanEqualAndDateLessThanEqual(startString, endString);
    }
    
    private LocalDate parseDate(String date) throws DateOutOfRangeException {
        LocalDate parsedDate = LocalDate.parse(date, DATE_FORMATTER);

        if (parsedDate.isBefore(DATE_MIN) || parsedDate.isAfter(LocalDate.now().minusDays(1))) {
            throw new DateOutOfRangeException();
        }
        
        return parsedDate;
    }
    
    @ExceptionHandler(DateTimeParseException.class)
    public ResponseEntity<Error> handleDateTimeParseException(DateTimeParseException e) {
        return ResponseEntity.badRequest().body(new Error(400, "invalid parameter", "invalid date"));
    }

    @ExceptionHandler(DateOutOfRangeException.class)
    public ResponseEntity<Error> handleDateOutOfRangeException(DateOutOfRangeException e) {
        return ResponseEntity.badRequest().body(new Error(400, "invalid parameter", 
                "date is out of allowed range from " + DATE_FORMATTER.format(DATE_MIN) + " until yesterday"));
    }

    @ExceptionHandler(InvalidRateIntervalException.class)
    public ResponseEntity<Error> handleInvalidRateIntervalException(InvalidRateIntervalException e) {
        return ResponseEntity.status(500).body(new Error(500, "internal error", "unexpected response from service"));
    }

    @ExceptionHandler(RestClientException.class)
    public ResponseEntity<Error> handleRestClientException(RestClientException e) {
        return ResponseEntity.status(500).body(new Error(500, "internal error", "service returns error"));
    }

    @ExceptionHandler(NumberFormatException.class)
    public ResponseEntity<Error> handleNumberFormatException(NumberFormatException e) {
        return ResponseEntity.status(400).body(new Error(400, "invalid parameter", "expected number"));
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<Error> handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException e) {
        return ResponseEntity.status(400).body(new Error(400, "invalid parameter", "invalid parameter " + 
                e.getName()));
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Error> handleException(Exception e) {
        return ResponseEntity.status(400).body(new Error(500, "internal error", "unknown error"));
    }

}
